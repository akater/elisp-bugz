# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: bugz
#+subtitle: Elisp interface to Bugzilla
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+description: Elisp interface to Bugzilla
#+property: header-args :tangle bugz.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

~bugz~ provides Elisp interface to Bugzilla, via ~pybugz~ only for now.

* Usage
Install pybugz, configure it as it suggests.  Here's an example config:
#+begin_example conf-unix
[Gentoo]
base = https://bugs.gentoo.org/xmlrpc.cgi
user = username@such.wow
password = abcde12345

[default]
connection = Gentoo
#+end_example

You may also use ~passwordcmd~ with the password extraction command rather than plain text ~password~ but in that case, if your ~passwordcmd~ involves submitting a password via pinentry, and you Emacs as your pinentry, make sure your PGP key password is cached in advance, as we might not be able to capture the PGP key password request request properly.

Install ~bugz~ and ~(require 'bugz)~.  See [[Suggested config]] below.

Visit a buffer with a bug number, point to the bug and try =M-x bugz-get-bug-at-point=.  If no buffer comes to mind, try =M-x bugz-hello=.

When you see the bug (the conversation about it, that is), try =M-x bugz-modify= in that buffer.

* Installation
** Gentoo
To install, please
#+begin_src sh :dir /sudo::/ :tangle no :results none
eselect repository enable akater
emerge app-emacs/bugz
#+end_src

** Other systems
Please use the =release= branch.

Ensure the following dependencies are installed:
- [[https://github.com/williamh/pybugz][pybugz]]
- [[https://framagit.org/akater/elisp-shmu][shmu]]
- (optionally, for running tests) [[https://gitlab.com/akater/org-development-elisp][org-development-elisp]]

Either add a package to your system or use our Makefile as is:
#+begin_src sh :tangle no :results none
make default && sudo make install
#+end_src

If your system doesn't have system-wide =site-lisp=, you may install like this:
#+begin_src sh :tangle no :results none
SITELISP=~/.config/emacs/lisp SITEETC=~/.config/emacs/etc make && DESTDIR=~/.config/emacs SITELISP=/lisp SITEETC=/etc make install
#+end_src

Elisp dependencies should be located in ~SITELISP~.

This will feel mostly like standard elpa install but you'll have to add load-path to your config:
#+begin_example elisp
(use-package bugz
  :load-path "~/.config/emacs/lisp/bugz"
  ..)
#+end_example

** Notes for contiributors
If you have a local version of the repository, compile with something like
#+begin_example sh :tangle no :results none
ORG_LOCAL_SOURCES_DIRECTORY="/home/contributor/src/emacs-bugz"
#+end_example

This will link definitions to your local Org files.

* Suggested config
#+begin_example elisp
(use-package bugz
  :custom
  (bugz-default-instance "Gentoo"
                         "The Bugzilla instance I primarily use")
  (bugz-backend 'pybugz)
  (bugz-directory (expand-file-name "bugz/" user-emacs-directory)))
#+end_example

* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'cl-macs))
(eval-when-compile (require 'mmxx-macros-anaphora))
(eval-when-compile (require 'mmxx-macros-applications))
(eval-when-compile (require 'bugz-macs))
#+end_src

* Features
** Core classes and high-level interface
#+begin_src elisp :results none
(require 'bugz-core)
#+end_src

** Buffers for posting and modifying bugs
#+begin_src elisp :results none
(require 'bugz-bugzpost)
#+end_src

** The default backend
#+begin_src elisp :results none
(require 'bugz-backend-pybugz)
#+end_src

* bugz-get-bug-at-point
** TODO Eliminate some repetition here and in bugz-bug-at-point
- State "TODO"       from              [2023-09-16 Sat 20:34]
** Definition
#+begin_src elisp :results none
(defun bugz-get-bug-at-point ()
  "Fetch a bug at point from the bug tracker."
  (interactive)
  (if-let ((bugid (bugz-bugid-at-point)))
      (bugz-get (or (bugz-guess-bug-tracker)
                    (bugz-read-bug-tracker))
                bugid)
    (message "bugz: I don't see any bug at point")
    nil))
#+end_src

** See Also
- [[file:bugz-core.org::bug-at-point][bug-at-point]]

* A “hello” function
#+begin_src elisp :results none
(require 'bugz-section)
(defun bugz-hello ()
  (interactive)
  (let ((buffer (get-buffer-create "bugz: hello")))
    (if-unsuccessfully (kill-buffer buffer)
      (with-current-buffer buffer
        (let ((inhibit-read-only t) (bugz-is-my-only-poster))
          (erase-buffer)
          (unless bugz-user-mail-address
            (setq-local bugz-user-mail-address
                        (read-string
                         "Enter your mail address to see bugs reported by you: ")))
          (insert "This is “hello” buffer for Elisp package bugz." ?\n)
          (insert "We're showing bugz on your default bug tracker," ?\n)
          (insert (format "reported by you (%s)." bugz-user-mail-address) ?\n)
          (insert "TAB folds the section, RET shows the bug." ?\n ?\n)
          (save-excursion
            (bugz-insert-bugz-reported-by-me bugz-default-instance t))
          (setq-local buffer-read-only t)
          (display-buffer (current-buffer)))))))
#+end_src

* Frontend
~bugz~ provides the following commands
- ~bugz-get~
- ~bugz-attachment~
- ~bugz-post~
- ~bugz-modify~
- ~bugz-attach~

and the function ~bugz-search~ which is not interactive yet.

* Backend
For now, we model requirements to backend on how our only backend pybugz works.  Thus, we require backend to provide functions: 
attach attachment connections (?) get modify post search

- backend should provide backend-primary-procedure get, post, modify, attach, attachment, search, async get

- backend get should return fresh buffer with formatted contents starting at point, ready to overwrite the conversation file.  The buffer may be killed by ~bugz~ immediately after the backend returns.

- backend attachment should return the name of the file saved by the backend

# consider (do not litter filesystem, and may be useful for tramp compatibility):
# - backend attachment should return multiple values: fresh buffer with attachment contents starting at point, and attachment name as string.  The buffer may be killed by ~bugz~ immediately after the backend returns.

- backend post should return multiple values (bugid bug-tracker buffer short-description) --- where buffer is fresh buffer with formatted contents starting at point, ready to overwrite the conversation file.  The buffer may be killed by ~bugz~ immediately after the backend returns.

- backend attach should return multiple values (attachid filename bugid bug-tracker) and should signal error ~bugz-bugzpost-attach-file-name-relative~ when filename in the bugzpost buffer is relative and ~bugz-bugzpost-attach-require-absolute-path~ is non-nil.  The error specifies the point where filename starts.

- backend modify should return freshly consed non-literal list
  (bug-tracker bugid plist)
  where plist is a plist with keys added modified-key describing changes.  An example of is ~(("priority" "---" "High"))~ — here, ~"---"~ is the old value, ~"High"~ is the new value.  Hopefully this gets specified better in future.

- primary procedures must have a specific signature: the first argument is always bug-tracker, the remaining arguments are as in an [[file:bugz-backend-pybugz.org::Definition][the definition of standard backend pybugz]]

- backend post, backend modify, backend attach should collect data from current buffer, do not try to guess anything (but maybe complete with backend-specific default values)

- backend search should return a list of plists, each plist corresponding to a bug found; the keys in plist must form a subset of initargs of bug object and must include ~:bug-tracker~ ~:bugid~ ~:description-maybe-truncated~, with values corresponding to the obvious properties of the bug.

backend primary procedures and parsers should signal errors
- provided by ~bugz-core~ and ~bugz-bugzpost~, like ~bugz-bugzpost-field-missing~, and
- (more general ones) provided by ~bugzilla~, like ~bugzilla-wrong-user~.

* TODO Maybe drop the requirement to provide “connections” in backends
- State "TODO"       from              [2023-05-19 Fri 10:14]
